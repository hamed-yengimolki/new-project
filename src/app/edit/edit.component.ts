import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { $ } from 'protractor';
@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  forms: any;
  closeResult: string;
  constructor(
    private http: HttpClient,
    // private modalService: NgbModal
  ) {
    this.forms = [];
  }

  ngOnInit(): void {
    this.http.get('../../assets/data/form.json')
      .subscribe((data) => {
        // console.log(data);
        this.forms = data;
      });
  }
}
