import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {
  lists: any;
  constructor(
    private http: HttpClient,
    private router: Router,
  ) {
    this.lists = [];
  }

  ngOnInit(): void {
    this.http.get('../../assets/data/list.json')
    .subscribe((data) => {
      // console.log(data);
      this.lists = data;
    });
  }
  logout() {
    this.router.navigate(['/']);
  }

}
