import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {
  tableHeader: any;
  constructor(
    private http: HttpClient,
  ) {
    this.tableHeader = [];
  }

  ngOnInit(): void {
    this.http.get('../../assets/data/table.json')
    .subscribe((data) => {
      this.tableHeader = data;
    });
  }

}
