import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { MainPageComponent } from './main-page/main-page.component';
import { TableComponent } from './table/table.component';
import { EditComponent } from './edit/edit.component';
const routes: Routes = [
  {
    path: '',
    component: LoginComponent
 },
 {
  path: 'mainPage',
  component: MainPageComponent,
  children: [
    {
      path: 'table/:id',
      component: TableComponent,
    },
    {
      path: 'edit/:id',
      component: EditComponent,
    }
  ]
}
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule { }
